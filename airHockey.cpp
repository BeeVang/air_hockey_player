/*
 ============================================================================
 Name        : airHockey.cpp
 Author      : Bee
 Version     : 1.00
 Copyright   : Your copyright notice
 Description : Air Hockey Software with gttkmm
 ============================================================================
 */

#include <gtkmm/main.h>
#include <gtk/gtk.h>
#include <windows.h>
#include "mainWindow.h"

using namespace std;

int main (int argc, char *argv[])
{
	Glib::thread_init();
	Gtk::Main kit(argc, argv);
	mainWindow mainWindow;

	std::cout << "main: start" << std::endl;
	//This is the main thread
	//Gtk::Main::run(mainWindow);
	kit.run(mainWindow);
	std::cout << "main: done" << std::endl;
	return 0;
}
