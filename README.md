# README #

This is a repo for my undergraduate research thesis. The project controls a 2-joint robotic arm to return an air hockey puck. The arm uses a webcam to detect the moving puck and performs a simpe state estimate. The state of the puck is used in a closed-loop feedback controller to control the end-effector of the robotic arm. This project also includes a GUI for ease of use.

Videos:

*  https://youtu.be/dtWqFiz_Sqs
*  https://youtu.be/cgQjwoB-q5k
*  https://youtu.be/8vRiHFn6sIM
*  https://youtu.be/r19ReOqIthU