/*
 * function.cpp
 *
 *  Created on: Jan 28, 2013
 *      Author: bvang13
 */

#include "function.h"

// functions and events
void mainWindow::connect_button(void)
{
	if (m_toggleConnect.get_active())//clicking connect
	{
		cport_nr = atoi(m_comboComport.get_active_text().c_str()) - 1;
		bdrate = atoi(m_comboBaudrate.get_active_text().c_str());
		if (indicator == 0)//phidget is not connected
		{
			encoderReader = encoder_simple();
			if (encoderReader != 0)//try to connect to phidget
			{
				indicator = 1;//phidget is connected
				m_textBuffer1->insert_at_cursor("Phidget Connected\n");
				m_textView1.set_buffer(m_textBuffer1);
				cout << "loaded settings" << endl;
				if(OpenComport(cport_nr, bdrate))//try to connect to arduino
				{
					m_textBuffer1->insert_at_cursor("ERROR: Cannot connect to Arduino\n");
					m_textView1.set_buffer(m_textBuffer1);
					errorMsg = true; //indicates that there is error connecting
					m_toggleConnect.set_active(false);
					runMotorController = false;//close PD control threads
				}
				else
				{//case where everything is connected
					m_textBuffer1->insert_at_cursor("Connected to Arduino\n");
					m_textView1.set_buffer(m_textBuffer1);
					m_toggleConnect.set_label("Disconnect");
					errorMsg = false; // no error connecting to comport
					//m_comboComport.set_editable(false);
					//m_comboBaudrate.set_editable(false);
				}
			}
			else//cannot connect to phidget
			{
				m_textBuffer1->insert_at_cursor("ERROR: Cannot connect to Phidget\n");
				m_textView1.set_buffer(m_textBuffer1);
				errorMsg = true;
				m_toggleConnect.set_active(false);
				runMotorController = false;//close PD control threads
			}
		}
		else//phidget already connected
		{
			m_textBuffer1->insert_at_cursor("Phidget Connected\n");
			m_textView1.set_buffer(m_textBuffer1);
			if(OpenComport(cport_nr, bdrate))
			{
				m_textBuffer1->insert_at_cursor("ERROR: Cannot connect to Arduino\n");
				m_textView1.set_buffer(m_textBuffer1);
				errorMsg = true; //indicates that there is error connecting
				m_toggleConnect.set_active(false);
				runMotorController = false;//close PD control threads
			}
			else
			{//case where everything is connected
				m_textBuffer1->insert_at_cursor("Connected to Arduino\n");
				m_textView1.set_buffer(m_textBuffer1);
				m_toggleConnect.set_label("Disconnect");
				errorMsg = false; // no error connecting to comport
				boost::thread(displayInfo);
				runMotorController = true;
				//m_comboComport.set_editable(false);
				//m_comboBaudrate.set_editable(false);
			}
		}
	}
	else//click disconnect
	{
		if (!errorMsg)
		{
			CloseComport(cport_nr);
			m_textBuffer1->insert_at_cursor("Disconnected from Arduino\n");
			m_textView1.set_buffer(m_textBuffer1);
			m_toggleConnect.set_label("Connect");
			//m_comboBaudrate.set_editable(true);
			//m_comboComport.set_editable(true);
			errorMsg = true;
			runMotorController = false;//close PD control threads
		}
	}
}

bool mainWindow::onKeyPress( GdkEventKey *event )
{
	if (!errorMsg)// only check keys if we are connected to the robot
	{
		int output = 1500;
		//base motor control
		if ( (event->keyval == GDK_KEY_s) || (event->keyval == 65464) )//numpad 8
		{
			cout << "up" << endl;
			output = 1505;
			itoa(output,vOut1,10);
			SendByte(cport_nr,'a');
			SendBuf(cport_nr,(unsigned char*)vOut1,4);
			SendByte(cport_nr,END_SIGNAL);
			Sleep(100);
			output = 1498;//offset so motor doesnt move
			itoa(output,vOut1,10);
			SendByte(cport_nr,'a');
			SendBuf(cport_nr,(unsigned char*)vOut1,4);
			SendByte(cport_nr,END_SIGNAL);
		}
		//base motor control
		else if ( (event->keyval == GDK_KEY_w) || (event->keyval == 65461) )//numpad 5
		{
			cout << "down" << endl;
			output = 1490;
			itoa(output,vOut1,10);
			SendByte(cport_nr,'a');
			SendBuf(cport_nr,(unsigned char*)vOut1,4);
			SendByte(cport_nr,END_SIGNAL);
			Sleep(100);
			output = 1498;//offset so motor doesnt move
			itoa(output,vOut1,10);
			SendByte(cport_nr,'a');
			SendBuf(cport_nr,(unsigned char*)vOut1,4);
			SendByte(cport_nr,END_SIGNAL);
		}
		//distal motor control
		else if ( (event->keyval == GDK_KEY_a) || (event->keyval == 65460) )//numpad 4
		{
			cout << "left" << endl;
			output = 1620;
			itoa(output,vOut2,10);
			SendByte(cport_nr,'b');
			SendBuf(cport_nr,(unsigned char*)vOut2,4);
			SendByte(cport_nr,END_SIGNAL);
			Sleep(10);
			output = 1500;
			itoa(output,vOut2,10);
			SendByte(cport_nr,'b');
			SendBuf(cport_nr,(unsigned char*)vOut2,4);
			SendByte(cport_nr,END_SIGNAL);

		}
		//distal motor control
		else if ( (event->keyval == GDK_KEY_d) || (event->keyval == 65462) )//numpad 6
		{
			cout <<  "right" << endl;
			output = 1370;
			itoa(output,vOut2,10);
			SendByte(cport_nr,'b');
			SendBuf(cport_nr,(unsigned char*)vOut2,4);
			SendByte(cport_nr,END_SIGNAL);
			Sleep(10);
			output = 1500;
			itoa(output,vOut2,10);
			SendByte(cport_nr,'b');
			SendBuf(cport_nr,(unsigned char*)vOut2,4);
			SendByte(cport_nr,END_SIGNAL);
		}
		updateMalletPositionDisplay();
	}

	//returning true, cancels the propagation of the event
	return true;
}


void mainWindow::on_button_clicked(Glib::ustring data)
{
	cout << "Hello World - " << data << " was pressed" << endl;
}

void mainWindow::save_log(void) //done for the most part
{
	ofstream myfile;
	time_t rawTime;
	mkdir("./logs/");
	struct tm * timeinfo;
	time(&rawTime);
	timeinfo = localtime( &rawTime);
	string filename = "./logs/ActionLog_";
	filename += asctime(timeinfo);
	replace(filename.begin(), filename.end(), ' ','_');
	replace(filename.begin(), filename.end(), ':','_');
	replace(filename.begin(), filename.end(), '\n','_');
	replace(filename.begin(), filename.end(), '\0','_');

	filename += ".txt";
	myfile.open (filename.c_str());
	if (myfile.is_open() )
	{
		myfile << m_textView1.get_buffer()->get_text();
		myfile.close();
		Gtk::MessageDialog dialog(*this, "Action log saved");
		Glib::ustring tempString = "Filename: ";
		tempString += filename;
		dialog.set_secondary_text(tempString);
		dialog.set_title("File Saved");
		dialog.run();
	}
	else
	{
		Gtk::MessageDialog dialog(*this, "Error");
		dialog.set_title("Error");
		dialog.set_secondary_text("The file could not be saved.");
	}
}

void mainWindow::help(void) //not done, only template
{
	dialog.show();
	dialog.set_position(Gtk::WIN_POS_CENTER);
}

void mainWindow::helpHide(void)
{
	dialog.hide();
}

void mainWindow::start_robot()
{
	if (m_toggleMode.get_active() && indicator != 0)
	{
		m_toggleMode.set_label("Stop");
		//startCameraThread
		runMotorController = true;
		boost::thread threadPD(displayInfo);
		m_textBuffer1->insert_at_cursor("Start PD control\n");
		m_textView1.set_buffer(m_textBuffer1);

		//start calc puck trajectory
		//commment out after 2.165 test
		runCalcPuckTrajectory = true;
		Glib::Thread::create(sigc::mem_fun(*this, &mainWindow::calculatePuckTrajectory),true);
		m_textBuffer1->insert_at_cursor("Start Calculate Puck Trajectory thread\n");
		m_textView1.set_buffer(m_textBuffer1);
	}
	else
	{
		m_toggleMode.set_label("Start");
		runMotorController = false;
		runCalcPuckTrajectory = false;
		m_textBuffer1->insert_at_cursor("Stopped PD control and CalcPuck Trajectory thread\n");
		m_textView1.set_buffer(m_textBuffer1);
	}
}

void mainWindow::startCameraThread(void)
{
	//cout << "thread camera started" << endl;
	m_textBuffer1->insert_at_cursor("Camera Tracking Started\n");
	m_textView1.set_buffer(m_textBuffer1);

	//check to see if camera is already tracking
	if (!cameraTracking)
	{
		cameraTracking = true;
		boost::thread thread3(cameraSensor);
	}
}

void mainWindow::updatePositions(void)
{
	while (true)
	{
		m_threadPosDisplay_Dispatcher();
		boost::this_thread::sleep(boost::posix_time::millisec(50));
	}
}

void mainWindow::updatePosDisplay(void)
{
	m_labelMalletX.set_text(Glib::ustring(malletX));
	m_labelMalletY.set_text(Glib::ustring(malletY));
	m_labelPuckX.set_text(Glib::ustring(puckX));
	m_labelPuckY.set_text(Glib::ustring(puckY));
	m_labelProjX.set_text(Glib::ustring(projectedX));
	m_labelProjY.set_text(Glib::ustring(projectedY));
}

void mainWindow::calibrateHSV(void)
{

	m_textBuffer1->insert_at_cursor("Calibrating HSV values\n");
	m_textView1.set_buffer(m_textBuffer1);
	cameraSensor.calibrateHSV();
}

void mainWindow::calibrateCamera(void)
{
	m_textBuffer1->insert_at_cursor("Calibrating Camera units\n");
	m_textView1.set_buffer(m_textBuffer1);
	//Glib::Thread::create(sigc::mem_fun(*this, &mainWindow::updateProjY),true);*/
	Glib::Thread::create(sigc::mem_fun(cameraSensor, &CameraSensor::calibrateCamera),true);
	//cameraSensor.calibrateCamera();
}

void mainWindow::setXYPos(void)
{
	double x = atof(m_inputDialogTheta1.get_text().c_str())/100.0;
	double y = atof(m_inputDialogTheta2.get_text().c_str())/100.0;
	double alpha = 0.0;
	if (x < 0)
	{
		alpha = M_PI - atan(abs(y/x));
	}
	else
	{
		alpha = atan(y/x);
	}

	currentTheta2 = M_PI - acos( (Len1*Len1 + Len2*Len2 - x*x - y*y)/(2*Len1*Len2));
	currentTheta1 = alpha - acos( (x*x + y*y + Len1*Len1 - Len2*Len2)/(2*Len1*sqrt(x*x + y*y)));
}

void mainWindow::setTheta2(void)
{
	//currentTheta2 = atof(m_inputDialogTheta2.get_text().c_str());
	currentTheta2 = 0.0;
	cout << "theta2 set to " << currentTheta2 << endl;
	CPhidgetEncoder_setPosition(encoderReader, 1,0);
	updateMalletPositionDisplay();
}

void mainWindow::goToXY(void)
{
	cout << "going to xy" << endl;
	double xe = atof(m_inputDialogX.get_text().c_str())/100.0;
	double ye = atof(m_inputDialogY.get_text().c_str())/100.0;
	setXY(xe,ye);
	cout << "t1 = " << desiredTheta1 << "t2 = " << desiredTheta2 << endl;
}

void mainWindow::calculatePuckTrajectory(void)
{
	double tempProjX = HOME_POS[0], tempProjY = HOME_POS[1];
	tableLength = atof(m_inputLength.get_text().c_str()),
			tableWidth = atof(m_inputWidth.get_text().c_str()),
			armOffset = atof(m_inputOffset.get_text().c_str());
	double timeStep = 0.15;
	//arm will only hit in between these two lines
	double yUpperHitLine = HOME_POS[1]+0.5;
	double yLowerHitLine = HOME_POS[1];
	cout << "Calc Puck Traj Thread started" << endl;
	//double xTime = 0.0, yTime = 0.0;
	double malletXPos = boost::lexical_cast<double>(malletX)/100.0;
	double malletYPos = boost::lexical_cast<double>(malletY)/100.0;
	double distanceBetweenPuckAndMallet = 0.0;
	while (runCalcPuckTrajectory)
	{

		if (puckVelocity[1] < -0.1) //puck is moving towards arm
		{
			//predict where puck will be in timeStep seconds
			//smart time step
			malletXPos = boost::lexical_cast<double>(malletX)/100.0;
			malletYPos = boost::lexical_cast<double>(malletY)/100.0;
			distanceBetweenPuckAndMallet = sqrt( (puckPositionX[2]-malletXPos)*(puckPositionX[2]-malletXPos) +
					(puckPositionY[2]-malletYPos)*(puckPositionY[2]-malletYPos))/abs(wcurrent2*(Len1+Len2));
			//cout << distanceBetweenPuckAndMallet << endl;
			cout << malletXPos << endl;
			if (distanceBetweenPuckAndMallet > .3)
			{
				timeStep = 0.15;
			}
			else
			{
				timeStep = 0.10;
			}
			tempProjX = puckVelocity[0]*timeStep + puckPositionX[2];
			tempProjY = puckVelocity[1]*timeStep + puckPositionY[2];
			//malletYPos = boost::lexical_cast<double>(malletY)/100.0;;
			if (abs(tempProjX) > (tableWidth/2-.12) || tempProjY < yLowerHitLine || tempProjY > yUpperHitLine)
			{
				//go to home position
				cout << "puck moving towards me, i'm going to home " << endl;
				projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
				projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
				setXY(HOME_POS[0], HOME_POS[1]);
			}
			else
			{
				//go to projected location
				cout << "go to proj loc" << endl;
				projectedX = boost::lexical_cast<string>(int(tempProjX*100));
				projectedY = boost::lexical_cast<string>(int(tempProjY*100));
				Glib::ustring tempString = "Moving to: (" + projectedX + "," + projectedY + ")\n";
				m_textBuffer1->insert_at_cursor(tempString);
				m_textView1.set_buffer(m_textBuffer1);
				setXY(tempProjX,tempProjY);
			}
		}
		else if ( (puckPositionY[2] < yUpperHitLine) && ( abs(puckVelocity[1]) < 0.05) && (puckPositionY[2] > yLowerHitLine) )
		{
			cout << "puck not moving" << endl;
			if (abs(puckVelocity[0]) < 0.05) //no movement in x or y
			{
				projectedX = boost::lexical_cast<string>(int(puckPositionX[2]*100));
				projectedY = boost::lexical_cast<string>(int(puckPositionY[2]*100));
				if (abs(puckPositionX[2]) > (tableWidth/2-.15))
				{
					//go to home position
					cout << "home 2" << endl;
					projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
					projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
					setXY(HOME_POS[0], HOME_POS[1]);
				}
				else
				{
					//go to projected location
					cout << "go to puck" << endl;
					projectedX = boost::lexical_cast<string>(int(puckPositionX[2]*100));
					projectedY = boost::lexical_cast<string>(int(puckPositionY[2]*100));
					setXY(puckPositionX[2],puckPositionY[2]);//go to puck's location, assume no movement
				}
			}
			else //puck is moving horizontally
			{
				tempProjX = puckVelocity[0]*timeStep + puckPositionX[2];
				projectedX = boost::lexical_cast<string>(int(tempProjX*100));
				projectedY = boost::lexical_cast<string>(int(puckPositionY[2]*100));
				setXY(tempProjX, puckPositionY[2]);
			}
		}
		else //puck is moving away or not on arm side
		{
			//go to home position
			cout << "im home" << endl;
			projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
			projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
			setXY(HOME_POS[0],HOME_POS[1]);//go to home position
		}
		//this sort of works
		/*//cout << puckVelocity[1] << endl;
		if (puckVelocity[1] < -0.1) //if puck is moving towards arm, y velocity is negative
		{
			//final goal: hit puck in hit-box
			cout<<"calcing traj" << endl;
			//find out where puck will hit wall, if any
			//x time to wall
			if (abs(puckVelocity[0]) < SPEED_TOL) //puck is not moving in x
			{
				xTime = 100000;//means we will never reach it
			}
			else //puck is moving in x
			{
				//xTime is when we'll hit either side of the table
				xTime = abs((tableWidth/2.0 - abs(puckPositionX[2]))/puckVelocity[0]);
			}

			//yTime to upper hit line
			yTime = (yUpperHitLine - puckPositionY[2])/puckVelocity[1];
			if (yTime < 0.0)
			{
				//puck is pass the upper hit line
				//check for lower hit line
				yTime = (yLowerHitLine - puckPositionY[2])/puckVelocity[1];
				if (yTime < 0.0)
				{
					yTime = 1000000;//means we will never reach it
				}
			}
			if (xTime < yTime) //hit sides first, ignore
			{
				//go to home position
				cout << "go home" << endl;
				projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
				projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
				setXY(HOME_POS[0], HOME_POS[1]);
			}
			else if (yTime < 2.0) //if puck will be between lines and yTime < 2.0 seconds
			{
				//hit puck where it'll be in 2sec
				tempProjX = puckVelocity[0]*0.1 + puckPositionX[2];
				tempProjY = puckVelocity[1]*0.1 + puckPositionY[2];
				if (abs(tempProjX) > (tableWidth/2-.15) || tempProjY < yLowerHitLine || tempProjY > yUpperHitLine)
				{
					//go to home position
					cout << "home 2" << endl;
					projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
					projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
					setXY(HOME_POS[0], HOME_POS[1]);
				}
				else
				{
					//go to projected location
					cout << "go to proj loc" << endl;
					projectedX = boost::lexical_cast<string>(int(tempProjX*100));
					projectedY = boost::lexical_cast<string>(int(tempProjY*100));
					Glib::ustring tempString = "Moving to: (" + projectedX + "," + projectedY + ")\n";
					m_textBuffer1->insert_at_cursor(tempString);
					m_textView1.set_buffer(m_textBuffer1);
					setXY(tempProjX,tempProjY);
				}
			}
			else
			{
				//go to home position
				cout << "home 3" << endl;
				projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
				projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
				setXY(HOME_POS[0], HOME_POS[1]);
			}
		}
		//if puck is on arm side and not moving towards arm
		else if ( (puckPositionY[2] < (armOffset + (tableLength/2.0))) && ( abs(puckVelocity[1]) < 0.05) && (puckPositionY[2] > yLowerHitLine) )
		{
			cout << "puck not moving" << endl;
			if (abs(puckVelocity[0]) < 0.05) //no movement in x or y
			{
				projectedX = boost::lexical_cast<string>(int(puckPositionX[2]*100));
				projectedY = boost::lexical_cast<string>(int(puckPositionY[2]*100));
				if (abs(puckPositionX[2]) > (tableWidth-.05))
				{
					//go to home position
					cout << "home 2" << endl;
					projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
					projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
					setXY(HOME_POS[0], HOME_POS[1]);
				}
				else
				{
					//go to projected location
					cout << "go to puck" << endl;
					projectedX = boost::lexical_cast<string>(int(puckPositionX[2]*100));
					projectedY = boost::lexical_cast<string>(int(puckPositionY[2]*100));
					setXY(puckPositionX[2],puckPositionY[2]);//go to puck's location, assume no movement
				}
			}
			else //puck is moving horizontally
			{
				tempProjX = puckVelocity[0]*2.0 + puckPositionX[2];
				projectedX = boost::lexical_cast<string>(int(tempProjX*100));
				projectedY = boost::lexical_cast<string>(int(puckPositionY[2]*100));
				setXY(tempProjX, puckPositionY[2]);
			}
		}
		else //puck is moving away or not on arm side
		{
			//go to home position
			cout << "im home" << endl;
			projectedX = boost::lexical_cast<string>(int(HOME_POS[0]*100));
			projectedY = boost::lexical_cast<string>(int(HOME_POS[1]*100));
			setXY(HOME_POS[0],HOME_POS[1]);//go to home position
		}*/
		boost::this_thread::sleep(boost::posix_time::millisec(50));
	}
	cout << "calc puck trajectory thread end" << endl;
}
