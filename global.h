/*
 * global.h
 *
 *  Created on: Jan 29, 2013
 *      Author: bvang13
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_
#define BOOST_THREAD_USE_LIB
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/math/special_functions/round.hpp>
#include <string>
#include <math.h>
#include <boost/timer.hpp>

using namespace std;

extern const double Len1, Len2, THETA_TOL1, THETA_TOL2, Kp1, Kd1, Kp2, Kd2, SPEED_TOL, HOME_POS[2];
extern const int V1_MAX, V1_MIN, V2_MAX, V2_MIN, V_NEUTRAL;
extern const unsigned char END_SIGNAL;
extern int cport_nr, bdrate;
extern double currentTheta1, currentTheta2, desiredTheta1, desiredTheta2,
				wcurrent1, wcurrent2, error1, error2;
extern int indicator;
extern double time1[3], time2[3], thetaStore1[3], thetaStore2[3], tableLength, tableWidth, armOffset;
extern char vOut1[], vOut2[];
extern bool runMotorController, runCalcPuckTrajectory, killThread, cameraTracking;

extern string malletX, malletY, puckX, puckY, projectedX, projectedY;

extern double calibratePoint1[4], calibratePoint2[4], calibratePoint3[4], calibratePoint4[4];
//using y = mx+b equation to find scale of px to real in the perspective of the camera
extern double calibrationXSlopePX[2], calibrationXConstantPX[2], calibrationYSlope, calibrationYConstant,
				calibrationXSlope, calibrationXConstant;

extern int encoderPosition[2]; //store encoder position until encoder is loaded

extern double puckTime[3], puckPositionX[3], puckPositionY[3], puckVelocity[2];

//velocity controller global variables
extern double lambda, xdot_r[2], qdot_r[2], x_d[2], x[2], xdot_d[2];
extern double R, c, t;
extern int output1, output2;

#endif /* GLOBAL_H_ */
