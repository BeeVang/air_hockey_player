/*
 * motorController.h
 *
 *  Created on: Jan 28, 2013
 *      Author: bvang13
 */

#ifndef MOTORCONTROLLER_H_
#define MOTORCONTROLLER_H_

#define BOOST_THREAD_USE_LIB
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <iostream>
#include <string>
#include <stdio.h>
#include <phidget21.h>
#include <windows.h>
#include <math.h>
#include "rs232.h"
#include "global.h"
using namespace std;

int CCONV AttachHandler(CPhidgetHandle ENC, void *userptr);
int CCONV DetachHandler(CPhidgetHandle ENC, void *userptr);
int CCONV ErrorHandler(CPhidgetHandle ENC, void *userptr, int ErrorCode, const char *Description);
int CCONV InputChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int State);
int CCONV PositionChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int Time, int RelativePosition);
int display_properties(CPhidgetEncoderHandle phid);
CPhidgetEncoderHandle encoder_simple();
void PDCtrl1();
void PDCtrl2();
void displayInfo();
void setXY(double x, double y);
void updateMalletPositionDisplay(void);
void VelocityController(void);


#endif /* MOTORCONTROLLER_H_ */
