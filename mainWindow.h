#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <gtkmm.h>
#include "CameraSensor.h"

using namespace std;

class mainWindow : public Gtk::Window
{
public:
  mainWindow();
  virtual ~mainWindow();

private:
  // Signal handlers:
  void on_button_clicked(Glib::ustring data);
  void connect_button(void);
  void save_log(void);
  void help(void);
  void helpHide(void);
  bool onKeyPress( GdkEventKey *event );
  void start_robot(void);
  void startCameraThread(void);
  void calibrateHSV(void);
  void loadHSV(void);
  void setXYPos(void);
  void setTheta2(void); //unused
  void goToXY(void);
  void updatePositions(void);
  void updatePosDisplay(void);
  void calibrateCamera(void);
  void calibrateAlgorithm(double xPX, double yPX);
  void calculatePuckTrajectory(void);

  // Child widgets:
  Gtk::Table m_Table1, m_Table2, m_Table3, m_Table4, m_tableSetting, m_tableStatus;
  Gtk::ToggleButton m_toggleConnect, m_toggleMode;
  Gtk::Button m_button2, m_button15, m_textSave, m_buttonHelpOk, m_buttonSetTheta1, m_buttonSetTheta2;
  Gtk::ScrolledWindow m_scrolledWindow;
  Gtk::TextView m_textView1;
  Glib::RefPtr<Gtk::TextBuffer> m_textBuffer1;
  Gtk::VBox m_VBox;
  Gdk::Color m_Color;
  Gtk::Frame m_frameStatus,m_frameConcSetting,m_frameButton, m_frameTest, m_frameMalletX, m_frameMalletY,
  	  	  	  	  m_framePuckX, m_framePuckY, m_frameProjX, m_frameProjY, m_frameArmSetting, m_frameCameraSetting, m_frameControl,
  	  	  	  	  m_frameTableSetting;
  Gtk::Label m_labelComport, m_labelBaudrate, m_labelArmPos, m_labelPuckPos, m_labelProjectedPos,
  	  	  	  	 m_labelArmSetting, m_labelCameraSetting, m_labelGoX, m_labelGoY, m_labelTableLength,
  	  	  	  	 m_labelTableWidth, m_labelArmOffset, m_labelMalletX, m_labelMalletY, m_labelPuckX,
  	  	  	  	 m_labelPuckY, m_labelProjX, m_labelProjY, m_labelSetX, m_labelSetY;
  Gtk::ComboBoxText m_comboComport, m_comboBaudrate;
  //Gtk::InputDialog m_inputDialogTheta1, m_inputDialogTheta2;
  Gtk::Entry m_inputDialogTheta1, m_inputDialogTheta2, m_inputDialogX, m_inputDialogY, m_inputLength,
  	  	  	  	  m_inputWidth, m_inputOffset;
  Gtk::Dialog dialog;
  Gtk::Label label1, label2,label3,label4;
  Gtk::Button m_buttonCameraCalibrate, m_buttonGoToPos, m_buttonCalibrateHSV, m_buttonStartCamera;
  Gtk::Table  m_tableTemp, m_tableAdvSetting, m_tableArmSetting, m_tableCameraSetting, m_tableControl,
  	  	  	  	  m_tableTableSetting;
  Gtk::Image m_imageMallet, m_imagePuck, m_imageGuess, m_imageControl;

  //display position thread
  Glib::Thread* m_threadPosDisplay;
  Glib::Dispatcher m_threadPosDisplay_Dispatcher;

  //class Variables
  bool errorMsg;
  CameraSensor cameraSensor;
  CPhidgetEncoderHandle encoderReader;
};

#endif // MAINWINDOW_H
