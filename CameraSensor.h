/*
 * CameraSensor.h
 *
 *  Created on: Jan 28, 2013
 *      Author: bvang13
 */

#ifndef CAMERASENSOR_H_
#define CAMERASENSOR_H_

#include <iostream>
#include <fstream>
#include "opencv2/opencv.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>
#include "motorController.h"

using namespace cv;
using namespace std;

class CameraSensor {
public:
	CameraSensor();
	virtual ~CameraSensor();
	void processImage(Mat frame);
	void operator()();
	void shutDown();
	void calibrateHSV();
	void updatePuckPosition();//need to finish, will track and update the position of the puck
	void calibrateCamera();//calibrate px to meters

private:
	void loadData(void);
	Mat image;
	bool killThread;
	int hsv_min[3], hsv_max[3];

};

#endif /* CAMERASENSOR_H_ */
