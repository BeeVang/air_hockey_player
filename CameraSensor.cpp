/*
 * CameraSensor.cpp
 *
 *  Created on: Jan 28, 2013
 *      Author: bvang13
 */

#include "CameraSensor.h"

CameraSensor::CameraSensor() {
	hsv_min[0] = 0;
	hsv_min[1] = 0;
	hsv_min[2] = 0;
	hsv_max[0] = 0;
	hsv_max[1] = 0;
	hsv_max[2] = 0;
	loadData();
}

CameraSensor::~CameraSensor() {
}

void CameraSensor::loadData(void)
{
	string line;
	int hsv[6];
	ifstream myfile ("hsvConfig.ini");
	if (myfile.is_open())
	{
		for (int i = 0; i < 6; i++)
		{
			getline (myfile, line);
			const char *num = line.c_str();
			hsv[i] = atoi( num );
		}
		hsv_min[0] = hsv[0];
		hsv_min[1] = hsv[1];
		hsv_min[2] = hsv[2];
		hsv_max[0] = hsv[3];
		hsv_max[1] = hsv[4];
		hsv_max[2] = hsv[5];
		myfile.close();
		cout << "hsvConfig.ini was loaded successfully." << endl;
	}
	else
	{
		cout << "Cannot open hsvConfig.ini or it does not exist." << endl;
	}

	//load all info of positionConfig.ini file
	ifstream myPos ("positionConfig.ini");
	if (myPos.is_open())
	{
		for (int i = 0; i < 24; i++)
		{
			getline (myPos, line);
			const char *num = line.c_str();
			if (i < 4)
			{
				calibratePoint1[i] = atof(num);
			}
			else if (i < 8)
			{
				calibratePoint2[i-4] = atof(num);
			}
			else if (i < 12)
			{
				calibratePoint3[i-8] = atof(num);
			}
			else if (i < 16)
			{
				calibratePoint4[i-12] = atof(num);
			}
			else if (i == 16)
			{
				calibrationYSlope = atof(num);
			}
			else if (i == 17)
			{
				calibrationYConstant = atof(num);
			}
			else if (i < 20)
			{
				calibrationXSlopePX[i-18] = atof(num);
			}
			else if (i < 22)
			{
				calibrationXConstantPX[i-20] = atof(num);
			}
			else if (i == 22)
			{
				currentTheta1 = atof(num);
				desiredTheta1 = currentTheta1;
			}
			else if (i == 23)
			{
				currentTheta2 = atof(num);
				desiredTheta2 = currentTheta2;
			}
		}
		myPos.close();
		cout << "positionConfig.ini was loaded successfully." << endl;
	}
	else
	{
		cout << "Cannot open positionConfig.ini or it does not exist." << endl;
	}
}


void CameraSensor::processImage(Mat frame)
{
	vector<vector<Point> > contours;
	Mat bimage = image >= 70;
	findContours(bimage, contours,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);
	Mat cimage = Mat::zeros(bimage.size(), CV_8UC3);

	double maxContour = -1.0;
	double maxArea = -1.0;
	for (size_t i = 0; i < contours.size(); i ++)
	{
		size_t count = contours[i].size();
		if ( count < 6 )
			continue;

		if ( contourArea(contours[i]) < maxArea)
			continue;

		maxArea = contourArea(contours[i]);
		maxContour = i;
	}

	//only print the largest circle's position
	if ((maxContour != -1.0) &&(maxArea != -1.0)) {
		Mat pointsf;
		Mat(contours[maxContour]).convertTo(pointsf, CV_32F);
		RotatedRect box = fitEllipse(pointsf);
		ellipse(frame,box.center,box.size*0.5f, box.angle, 0, 360, Scalar(0,256,0),1,CV_AA);
		cout <<box.center<<endl;
	}
}

void CameraSensor::calibrateHSV()
{
	//Image variables
	CvCapture* capture = cvCaptureFromCAM( 1 );
	int h1=hsv_min[0];int s1=hsv_min[1];int v1=hsv_min[2];
	int h2=hsv_max[0];int s2=hsv_max[1];int v2=hsv_max[2];
	CvSize size = cvSize(640,480);
	cvNamedWindow("cnt",CV_WINDOW_AUTOSIZE);
	//track bars
	cvCreateTrackbar("H1","cnt",&h1,510,0);
	cvCreateTrackbar("S1","cnt",&s1,510,0);
	cvCreateTrackbar("V1","cnt",&v1,510,0);
	cvCreateTrackbar("H2","cnt",&h2,510,0);
	cvCreateTrackbar("S2","cnt",&s2,510,0);
	cvCreateTrackbar("V2","cnt",&v2,510,0);
	IplImage* hsvimg=cvCreateImage(size,IPL_DEPTH_8U,3);
	IplImage* thresh=cvCreateImage(size,IPL_DEPTH_8U,1);
	IplImage* frame = cvQueryFrame( capture );
	if( !capture )
	{
		fprintf( stderr, "ERROR: capture is NULL \n" );
		getchar();
		return;
		//return -1;
	}
	while(true)
	{
		frame= cvQueryFrame(capture);
		if( !frame )
		{
			fprintf( stderr, "ERROR: frame is null...\n" );
			getchar();
			break;
		}
		//Windows
		cvNamedWindow("Original Image",CV_WINDOW_AUTOSIZE);
		cvNamedWindow("Thresholded Image",CV_WINDOW_AUTOSIZE);
		//Changing into HSV plane
		cvCvtColor(frame,hsvimg,CV_BGR2HSV);
		//Thresholding the image
		cvInRangeS(hsvimg,cvScalar(h1,s1,v1),cvScalar(h2,s2,v2),thresh);
		//Showing the images
		cvShowImage("Original Image",frame);
		cvShowImage("Thresholded Image",thresh);
		//Waiting for user to press any key
		if( (cvWaitKey(10) & 255) == 27 ) break;
	}
	hsv_min[0] = h1;
	hsv_min[1] = s1;
	hsv_min[2] = v1;
	hsv_max[0] = h2;
	hsv_max[1] = s2;
	hsv_max[2] = v2;
	ofstream myfile ("hsvConfig.ini");
	if (myfile.is_open())
	{
		myfile << h1 <<"\n";
		myfile << s1 <<"\n";
		myfile << v1 <<"\n";
		myfile << h2 <<"\n";
		myfile << s2 <<"\n";
		myfile << v2 <<"\n";
		myfile.close();
		cout << "Configuration file updated." << endl;
	}
	else
	{
		cout << "Cannot write to file." << endl;
	}
	cvReleaseCapture( &capture);
	cvDestroyAllWindows();
}

void CameraSensor::calibrateCamera()
{
	// Default capture size - 640x480
	CvSize size = cvSize(640,480);
	// Open capture device. 0 is /dev/video0, 1 is /dev/video1, etc.
	CvCapture* capture = cvCaptureFromCAM( 1 );
	if( !capture )
	{
		fprintf( stderr, "ERROR: capture is NULL \n" );
		getchar();
		return;
	}
	// Create a window in which the captured images will be presented
	cvNamedWindow( "Camera", CV_WINDOW_AUTOSIZE );
	// Detect puck color
	CvScalar hsv_min_scalar = cvScalar(hsv_min[0], hsv_min[1], hsv_min[2]);
	CvScalar hsv_max_scalar = cvScalar(hsv_max[0], hsv_max[1], hsv_max[2]);
	IplImage*  hsv_frame    = cvCreateImage(size, IPL_DEPTH_8U, 3);
	IplImage*  thresholded   = cvCreateImage(size, IPL_DEPTH_8U, 1);
	IplImage* frame = cvQueryFrame(capture);
	while( true )
	{
		// Get one frame
		frame = cvQueryFrame( capture );
		if( !frame )
		{
			fprintf( stderr, "ERROR: frame is null...\n" );
			getchar();
			break;
		}
		// Covert color space to HSV as it is much easier to filter colors in the HSV color-space.
		cvCvtColor(frame, hsv_frame, CV_BGR2HSV);
		// Filter out colors which are out of range.
		cvInRangeS(hsv_frame, hsv_min_scalar, hsv_max_scalar, thresholded);
		// Memory for hough circles
		CvMemStorage* storage = cvCreateMemStorage(0);
		// hough detector works better with some smoothing of the image
		cvSmooth( thresholded, thresholded, CV_GAUSSIAN, 9, 9 );
		image = Mat(thresholded);
		//processImage(Mat(frame));
		vector<vector<Point> > contours;
		Mat bimage = image >= 70;
		findContours(bimage, contours,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);
		Mat cimage = Mat::zeros(bimage.size(), CV_8UC3);
		double maxContour = -1.0;
		double maxArea = -1.0;
		for (size_t i = 0; i < contours.size(); i ++)
		{
			size_t count = contours[i].size();
			if ( count < 6 )
				continue;

			if ( contourArea(contours[i]) < maxArea)
				continue;

			maxArea = contourArea(contours[i]);
			maxContour = i;
		}
		Mat tempFrame = Mat(frame);
		//only print the largest circle's position
		RotatedRect box;
		if ((maxContour != -1.0) &&(maxArea != -1.0)) {
			Mat pointsf;
			Mat(contours[maxContour]).convertTo(pointsf, CV_32F);
			box = fitEllipse(pointsf);
			ellipse(tempFrame,box.center,box.size*0.5f, box.angle, 0, 360, Scalar(0,256,0),1,CV_AA);
			//cout <<box.center<<endl;
		}
		cvShowImage( "Camera", frame ); // Original stream with detected ball overlay
		//cvShowImage( "HSV&", hsv_frame); // Original stream in the HSV color space
		cvShowImage( "After Color Filtering", thresholded ); // The stream after color filtering
		cvReleaseMemStorage(&storage);
		// Do not release the frame!
		int keyPressed = cvWaitKey(10)&255;
		//cout<<keyPressed<<endl;
		if( keyPressed == 27 )//esc key
		{
			double yAvg[4];
			yAvg[0] = (calibratePoint1[1] + calibratePoint2[1])/2.0; //avg Y px for bottom
			yAvg[1] = (calibratePoint1[3] + calibratePoint2[3])/2.0; //avg Y real for bottm
			yAvg[2] = (calibratePoint3[1] + calibratePoint4[1])/2.0; //avg Y px for top
			yAvg[3] = (calibratePoint3[3] + calibratePoint4[3])/2.0; //avg Y real for top

			//finding line equation for Y conversion, this won't change
			calibrationYSlope = (yAvg[1]-yAvg[3])/(yAvg[0]-yAvg[2]); // slope real/px
			calibrationYConstant = yAvg[1] - calibrationYSlope*yAvg[0]; //constant for Y equation

			//finding x px lines for left and right
			//assume x are of same length in x and y direction
			//x is dependent, y is given
			calibrationXSlopePX[0] = (calibratePoint1[0]-calibratePoint3[0])/(calibratePoint1[1]-calibratePoint3[1]);//using left points
			calibrationXSlopePX[1] = (calibratePoint2[0]-calibratePoint4[0])/(calibratePoint2[1]-calibratePoint4[1]);//using right points
			calibrationXConstantPX[0] = calibratePoint1[0] - calibrationXSlopePX[0]*calibratePoint1[1];
			calibrationXConstantPX[1] = calibratePoint2[0] - calibrationXSlopePX[1]*calibratePoint2[1];
			//found xpx = f(ypx), this will allow us to see how the real length scales in the camera view

			cout <<"Calibration done!" << endl;
			break;
		}
		else if ( keyPressed == 106) //j is pressed, bottom left
		{
			calibratePoint1[0] = box.center.x; //px X
			calibratePoint1[1] = box.center.y; //px Y
			calibratePoint1[2] = boost::lexical_cast<double>(malletX)/100.0; //real x (m)
			calibratePoint1[3] = boost::lexical_cast<double>(malletY)/100.0; //real y (m)
			cout<<"calibration point 1 stored" << endl;

		}
		else if ( keyPressed == 107) //k is pressed, bottom right
		{
			calibratePoint2[0] = box.center.x; //px X
			calibratePoint2[1] = box.center.y; //px Y
			calibratePoint2[2] = boost::lexical_cast<double>(malletX)/100.0; //real x (m)
			calibratePoint2[3] = boost::lexical_cast<double>(malletY)/100.0; //real y (m)
			cout<<"calibration point 2 stored" << endl;
		}
		/*		else if ( keyPressed == 108) //l is pressed
		{

		}*/
		else if ( keyPressed == 117) //u is pressed, top left
		{
			calibratePoint3[0] = box.center.x; //px X
			calibratePoint3[1] = box.center.y; //px Y
			calibratePoint3[2] = boost::lexical_cast<double>(malletX)/100.0; //real x (m)
			calibratePoint3[3] = boost::lexical_cast<double>(malletY)/100.0; //real y (m)
			cout<<"calibration point 3 stored" << endl;
		}
		else if ( keyPressed == 105) //i is pressed, top right
		{
			calibratePoint4[0] = box.center.x; //px X
			calibratePoint4[1] = box.center.y; //px Y
			calibratePoint4[2] = boost::lexical_cast<double>(malletX)/100.0; //real x (m)
			calibratePoint4[3] = boost::lexical_cast<double>(malletY)/100.0; //real y (m)
			cout<<"calibration point 4 stored" << endl;
		}
		/*else if ( keyPressed == 111) //o is pressed
		{

		}*/

	}
	cvDestroyAllWindows();
	cvReleaseCapture( &capture );
}

void CameraSensor::shutDown()
{
	killThread = true;
}

//detect puck and convert its pixel position into meters
void CameraSensor::operator()()
{
	// Default capture size - 640x480
	CvSize size = cvSize(640,480);
	// Open capture device. 0 is /dev/video0, 1 is /dev/video1, etc.
	CvCapture* capture = cvCaptureFromCAM( 1 );
	if( !capture )
	{
		fprintf( stderr, "ERROR: capture is NULL \n" );
		getchar();
		return;
	}
	// Create a window in which the captured images will be presented
	cvNamedWindow( "Camera", CV_WINDOW_AUTOSIZE );
	// Detect puck
	CvScalar hsv_min_scalar = cvScalar(hsv_min[0], hsv_min[1], hsv_min[2]);
	CvScalar hsv_max_scalar = cvScalar(hsv_max[0], hsv_max[1], hsv_max[2]);
	IplImage*  hsv_frame    = cvCreateImage(size, IPL_DEPTH_8U, 3);
	IplImage*  thresholded   = cvCreateImage(size, IPL_DEPTH_8U, 1);
	IplImage* frame = cvQueryFrame( capture );
	//reusable variables
	double yPxCurrent = 0.0, xPxCurrent = 0.0, xPx[2] = {0.0,0.0};
	boost::timer puckTimer; //start timer to keep track of time for tracking puck
	while( true )
	{
		// Get one frame
		frame = cvQueryFrame( capture );
		if( !frame )
		{
			fprintf( stderr, "ERROR: frame is null...\n" );
			getchar();
			break;
		}
		// Covert color space to HSV as it is much easier to filter colors in the HSV color-space.
		cvCvtColor(frame, hsv_frame, CV_BGR2HSV);
		// Filter out colors which are out of range.
		cvInRangeS(hsv_frame, hsv_min_scalar, hsv_max_scalar, thresholded);
		// Memory for hough circles
		CvMemStorage* storage = cvCreateMemStorage(0);
		// hough detector works better with some smoothing of the image
		cvSmooth( thresholded, thresholded, CV_GAUSSIAN, 9, 9 );
		image = Mat(thresholded);
		vector<vector<Point> > contours;
		Mat bimage = image >= 70;
		findContours(bimage, contours,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);
		Mat cimage = Mat::zeros(bimage.size(), CV_8UC3);
		double maxContour = -1.0;
		double maxArea = -1.0;
		for (size_t i = 0; i < contours.size(); i ++)
		{
			size_t count = contours[i].size();
			if ( count < 6 )
				continue;

			if ( contourArea(contours[i]) < maxArea)
				continue;

			maxArea = contourArea(contours[i]);
			maxContour = i;
		}
		Mat tempFrame = Mat(frame);
		//only print the largest circle's position
		RotatedRect box;
		//reset location
		xPxCurrent = -1.0;
		yPxCurrent = -1.0;
		if ((maxContour != -1.0) &&(maxArea != -1.0)) {
			Mat pointsf;
			Mat(contours[maxContour]).convertTo(pointsf, CV_32F);
			box = fitEllipse(pointsf);
			ellipse(tempFrame,box.center,box.size*0.5f, box.angle, 0, 360, Scalar(0,256,0),1,CV_AA);
			xPxCurrent = box.center.x;
			yPxCurrent = box.center.y;
		}
		//store x positions
		puckPositionX[0] = puckPositionX[1];
		puckPositionX[1] = puckPositionX[2];
		//store y positions
		puckPositionY[0] = puckPositionY[1];
		puckPositionY[1] = puckPositionY[2];
		//store time elapsed since we started tracking
		puckTime[0] = puckTime[1];
		puckTime[1] = puckTime[2];
		puckTime[2] = puckTimer.elapsed();//seconds
		if (xPxCurrent != -1.0 && yPxCurrent != -1.0)
		{
			//calc puck position base on camera
			//first find x length base on pixels
			xPx[0] = calibrationXSlopePX[0]*yPxCurrent + calibrationXConstantPX[0]; //left side x px
			xPx[1] = calibrationXSlopePX[1]*yPxCurrent + calibrationXConstantPX[1]; //right side x px
			//now find xreal = f(xpx) using calibration length and left/right x px, result = m and b
			calibrationXSlope = (calibratePoint1[2]-calibratePoint2[2])/(xPx[0]-xPx[1]);
			calibrationXConstant = calibratePoint1[2] - calibrationXSlope*xPx[0];
			//find what current xPx and yPx of camera are in real units

			puckPositionX[2] = calibrationXSlope*xPxCurrent + calibrationXConstant;//m
			puckPositionY[2] = calibrationYSlope*yPxCurrent + calibrationYConstant;//m

			//velocity vector
			puckVelocity[0] = (puckPositionX[2] - puckPositionX[0])/(puckTime[2]-puckTime[1]);//x dir
			puckVelocity[1] = (puckPositionY[2] - puckPositionY[0])/(puckTime[2]-puckTime[1]);//y dir

			if (puckVelocity[0] > 10.0)
			{
				puckVelocity[0] = 10.0;
			}
			else if (puckVelocity[0] < -10.0)
			{
				puckVelocity[0] = -10.0;
			}

			if (puckVelocity[1] > 10.0)
			{
				puckVelocity[1] = 10.0;
			}
			else if (puckVelocity[1] < -10.0)
			{
				puckVelocity[1] = -10.0;
			}

		}
		else //no puck found, will direct arm to home location
		{
			//set puck position to home
			puckPositionX[2] = HOME_POS[0];
			puckPositionY[2] = HOME_POS[1];

			//set puck velocity to zero
			puckVelocity[0] = 0.0;
			puckVelocity[1] = 0.0;
		}
		puckX = boost::lexical_cast<string>(int(puckPositionX[2]*100)); // (cm)
		puckY = boost::lexical_cast<string>(int(puckPositionY[2]*100)); // (cm)
		cvShowImage( "Camera", frame ); // Original stream with detected ball overlay
		//cvShowImage( "HSV&", hsv_frame); // Original stream in the HSV color space
		cvShowImage( "After Color Filtering", thresholded ); // The stream after color filtering
		cvReleaseMemStorage(&storage);
		// Do not release the frame!
		if ( (cvWaitKey(10)&255) == 27)
		{
			cameraTracking = false;
			break;
		}
		//if (killThread)	break;

	}
	cvDestroyAllWindows();
	cvReleaseCapture( &capture );
}
