/*
 * motorController.cpp
 *
 *  Created on: Jan 28, 2013
 *      Author: bvang13
 */

#include "motorController.h"

int CCONV AttachHandler(CPhidgetHandle ENC, void *userptr)
{
	int serialNo;
	CPhidget_DeviceID deviceID;
	int i, inputcount;

	CPhidget_getSerialNumber(ENC, &serialNo);

	//Retrieve the device ID and number of encoders so that we can set the enables if needed
	CPhidget_getDeviceID(ENC, &deviceID);
	CPhidgetEncoder_getEncoderCount((CPhidgetEncoderHandle)ENC, &inputcount);
	printf("Encoder %10d attached! \n", serialNo);

	//the 1047 requires enabling of the encoder inputs, so enable them if this is a 1047
	if (deviceID == PHIDID_ENCODER_HS_4ENCODER_4INPUT)
	{
		printf("Encoder requires Enable. Enabling inputs....\n");
		for (i = 0 ; i < inputcount ; i++)
			CPhidgetEncoder_setEnabled((CPhidgetEncoderHandle)ENC, i, 1);
	}
	return 0;
}


int CCONV DetachHandler(CPhidgetHandle ENC, void *userptr)
{
	int serialNo;
	CPhidget_getSerialNumber(ENC, &serialNo);
	cout << "Device: " << serialNo << " is detached." << endl;
	indicator = 0;
	return 0;
}

int CCONV ErrorHandler(CPhidgetHandle ENC, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s \n", ErrorCode, Description);

	return 0;
}

int CCONV InputChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int State)
{
	printf("Input #%i - State: %i \n", Index, State);

	return 0;
}


int CCONV PositionChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int Time, int RelativePosition)
{
	int Position;
	CPhidgetEncoder_getPosition(ENC, Index, &Position);
	//printf("Encoder #%i - Position: %5d -- Relative Change %2d -- Elapsed Time: %5d \n", Index, Position, RelativePosition, Time);
	if (Index == 0)
	{//this is the base arm encoder
		//currentTheta1 = M_PI/2 + (double)Position*(2.0*M_PI/31000.0);//pi/2 + count*conversion factor of CPR to rad
		currentTheta1 += (double)RelativePosition*(2.0*M_PI/31000.0);
		time1[0] = time1[1];
		time1[1] = time1[2];
		time1[2] = (double) Time/1000.0;//Time is in ms, time1[2] is in seconds
		thetaStore1[0] = thetaStore1[1];
		thetaStore1[1] = thetaStore1[2];
		thetaStore1[2] = currentTheta1;
		encoderPosition[0] = Position;
	}
	else if (Index == 1)
	{//this is the distal encoder
		currentTheta2 -= (double)RelativePosition*(2.0*M_PI/4330.88);
		time2[0] = time2[1];
		time2[1] = time2[2];
		time2[2] = (double) Time/1000.0;//Time is in ms, time2[2] is in seconds
		thetaStore2[0] = thetaStore2[1];
		thetaStore2[1] = thetaStore2[2];
		thetaStore2[2] = currentTheta2;
		encoderPosition[1] = Position;
	}
	return 0;
}

//Display the properties of the attached phidget to the screen.  We will be displaying the name, serial number and version of the attached device.
//Will also display the number of inputs and encoders on this device
int display_properties(CPhidgetEncoderHandle phid)
{
	int serialNo, version, num_inputs, num_encoders;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetEncoder_getInputCount(phid, &num_inputs);
	CPhidgetEncoder_getEncoderCount(phid, &num_encoders);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("Num Encoders: %d\nNum Inputs: %d\n", num_encoders, num_inputs);

	return 0;
}

CPhidgetEncoderHandle encoder_simple()
{
	int result;
	const char *err;

	//Declare an encoder handle
	CPhidgetEncoderHandle encoder = 0;
	//create the encoder object
	CPhidgetEncoder_create(&encoder);

	//Set the handlers to be run when the device is plugged in or opened from software, unplugged or closed from software, or generates an error.
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)encoder, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)encoder, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)encoder, ErrorHandler, NULL);

	//Registers a callback that will run if an input changes.
	//Requires the handle for the Phidget, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetEncoder_set_OnInputChange_Handler(encoder, InputChangeHandler, NULL);

	//Registers a callback that will run if the encoder changes.
	//Requires the handle for the Encoder, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetEncoder_set_OnPositionChange_Handler (encoder, PositionChangeHandler, NULL);

	CPhidget_open((CPhidgetHandle)encoder, -1);

	//get the program to wait for an encoder device to be attached
	cout << "Waiting for encoder to be attached...." << endl;
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)encoder, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		cout << "Problem waiting for attachment: " << err << endl;
	}

	//Display the properties of the attached encoder device
	display_properties(encoder);

	//read encoder event data
	cout <<"Reading....." << endl;
	return encoder;
}

void VelocityController()
{// an adaptive controller to move the arm in a circle, to use call this in the displayInfo() function
	wcurrent1 = (thetaStore1[2]-thetaStore1[0])/(time1[2]-time1[0]);
	wcurrent2 = (thetaStore2[2]-thetaStore2[0])/(time2[2]-time2[0]);

	//velocity controller
	xdot_d[0] = -c*R*sin(c*t);
	xdot_d[1] = c*R*cos(c*t);
	x[0] = Len1*cos(currentTheta1)+Len2*cos(currentTheta1+currentTheta2);//m
	x[1] = Len1*sin(currentTheta1)+Len2*sin(currentTheta1+currentTheta2);//m
	x_d[0] = R*cos(c*t);
	x_d[1] = R*sin(c*t)+.4;

	xdot_r[0] = xdot_d[0]-lambda*(x[0]-x_d[0]);
	xdot_r[1] = xdot_d[1]-lambda*(x[1]-x_d[1]);

	qdot_r[0] = 1/(Len1*Len2*sin(currentTheta2))*
			(Len2*cos(currentTheta1+currentTheta2)*xdot_r[0]+Len2*sin(currentTheta1+currentTheta2)*xdot_r[1]);
	qdot_r[1] = 1/(Len1*Len2*sin(currentTheta2))*
			((-Len1*cos(currentTheta1)-Len2*cos(currentTheta1+currentTheta2))*xdot_r[0]-
					(Len1*sin(currentTheta1)+Len2*sin(currentTheta1+currentTheta2))*xdot_r[1]);

	//determine outputs

	//control signal to shoulder motor
	output1 = int(-21*qdot_r[0])+1500;

	/*output1 -= int(5*(-wcurrent1+qdot_r[0]));
	if (output1 > V1_MAX) output1 = V1_MAX;
	if (output1 < V1_MIN) output1 = V1_MIN;
*/
	itoa(output1,vOut1,10);//assign vOut2 to be the output value (from int to char[])
	//puckX = boost::lexical_cast<string>(int(wcurrent1*100)); // (cm)
	puckX = boost::lexical_cast<string>(output1); // (cm)
	//cout << "output1 = " << output << endl;
	SendByte(cport_nr,'a');//forearm motor indicator
	SendBuf(cport_nr,(unsigned char*)vOut1,4);//send motor value
	//cout << (unsigned char*)vOut1 <<endl;
	SendByte(cport_nr,END_SIGNAL);//send end of transmission signal
	boost::this_thread::sleep(boost::posix_time::millisec(20));

	//control signal to distal motor
	output2 = int(100*qdot_r[1])+1500;

	/*output2 += int(10*(-wcurrent2+qdot_r[1]));
	if (output2 > V2_MAX) output2 = V2_MAX;
	if (output2 < V2_MIN) output2 = V2_MIN;
*/
	itoa(output2,vOut2,10);//assign vOut2 to be the output value (from int to char[])
	//cout << "output2 = " << output << endl;
	SendByte(cport_nr,'b');//forearm motor indicator
	SendBuf(cport_nr,(unsigned char*)vOut2,4);//send motor value
	SendByte(cport_nr,END_SIGNAL);//send end of transmission signal
	//puckY = boost::lexical_cast<string>(int(wcurrent2*100)); // (cm)
	puckY = boost::lexical_cast<string>(output2); // (cm)
	projectedX = boost::lexical_cast<string>(int(qdot_r[0]*100));
	projectedY = boost::lexical_cast<string>(int(qdot_r[1]*100));
	boost::this_thread::sleep(boost::posix_time::millisec(20));
}


void PDCtrl1()
{//pd ctrl for base arm
	wcurrent1 = 1/(time1[2]-time1[0])*(thetaStore1[2]-thetaStore1[0]);
	error1 = desiredTheta1 - currentTheta1;
	int output = 1498;//offset
	output = 1500 - (Kp1*error1 + Kd1*wcurrent1);//generate pwm signal to send
	//check to make sure velocities are below the limits
	if (output >= V1_MAX) {
		output = V1_MAX;
	}
	else if (output <= V1_MIN ) {
		output = V1_MIN;
	}
	itoa(output,vOut1,10);//assign vOut2 to be the output value (from int to char[])

	//cout << "output1 = " << output << endl;
	SendByte(cport_nr,'a');//forearm motor indicator
	SendBuf(cport_nr,(unsigned char*)vOut1,4);//send motor value
	//cout << (unsigned char*)vOut1 <<endl;
	SendByte(cport_nr,END_SIGNAL);//send end of transmission signal
}


void PDCtrl2()
{//pd ctrl for forearm
	wcurrent2 = -(thetaStore2[2]-thetaStore2[0])/(time2[2]-time2[0]);
	error2 = desiredTheta2 - currentTheta2;
	int output = 1500;
	output = 1500 + (Kp2*error2 + Kd2*wcurrent2);//generate pwm signal to send
	//projectedY = boost::lexical_cast<string>(output);
	//check to make sure velocities are below the limits
	if (output >= V2_MAX) {
		output = V2_MAX;
	}
	else if (output <= V2_MIN ) {
		output = V2_MIN;
	}
	itoa(output,vOut2,10);//assign vOut2 to be the output value (from int to char[])
	//cout << "output2 = " << output << endl;
	SendByte(cport_nr,'b');//forearm motor indicator
	SendBuf(cport_nr,(unsigned char*)vOut2,4);//send motor value
	SendByte(cport_nr,END_SIGNAL);//send end of transmission signal
}

void setXY(double x, double y) //takes in meters
{
	double alpha = 0.0;
	if (x < 0)
	{
		alpha = M_PI - atan(abs(y/x));
	}
	else
	{
		alpha = atan(y/x);
	}

	desiredTheta2 = M_PI - acos( (Len1*Len1 + Len2*Len2 - x*x - y*y)/(2*Len1*Len2));
	desiredTheta1 = alpha - acos( (x*x + y*y + Len1*Len1 - Len2*Len2)/(2*Len1*sqrt(x*x + y*y)));
}

void updateMalletPositionDisplay(void)
{
	malletX = boost::lexical_cast<string>(int((Len1*cos(currentTheta1)+Len2*cos(currentTheta1+currentTheta2))*100.0));
	malletY = boost::lexical_cast<string>(int((Len1*sin(currentTheta1)+Len2*sin(currentTheta1+currentTheta2))*100.0));
}

void displayInfo()
{
	cout << "dislpayInfo() created" << endl;
	boost::timer controllerTimer;
	while (true)
	{
		PDCtrl1();
		PDCtrl2();
		//t = controllerTimer.elapsed();
		//VelocityController();
		updateMalletPositionDisplay();
		//cout<<"Theta1: "<<currentTheta1<<" Theta2: " <<currentTheta2<<endl;
		if (!runMotorController)
		{
			break;//end this thread
			cout << "displayInfo() tread killed" << endl;
		}

		boost::this_thread::sleep(boost::posix_time::millisec(10));
	}
	output1 = 1500;
	itoa(output1,vOut1,10);//assign vOut2 to be the output value (from int to char[])
	//cout << "output1 = " << output << endl;
	SendByte(cport_nr,'a');//forearm motor indicator
	SendBuf(cport_nr,(unsigned char*)vOut1,4);//send motor value
	//cout << (unsigned char*)vOut1 <<endl;
	SendByte(cport_nr,END_SIGNAL);//send end of transmission signal
	boost::this_thread::sleep(boost::posix_time::millisec(20));
	//control signal to distal motor
	output2 = 1500;
	itoa(output2,vOut2,10);//assign vOut2 to be the output value (from int to char[])
	//cout << "output2 = " << output << endl;
	SendByte(cport_nr,'b');//forearm motor indicator
	SendBuf(cport_nr,(unsigned char*)vOut2,4);//send motor value
	SendByte(cport_nr,END_SIGNAL);//send end of transmission signal
	cout << "displayInfo() finished" << endl;
}
