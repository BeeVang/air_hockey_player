/*
 * global.cpp
 *
 *  Created on: Jan 29, 2013
 *      Author: bvang13
 */

#include "global.h"

//constants
const double Len1 = 0.55834526;//base arm length in meters
const double Len2 = 0.43815;//forearm length in meters
const double THETA_TOL1 = 0.1;//radian
const double THETA_TOL2 = 0.1;//radian
const int V1_MAX = 1510;//base max (1520)
const int V1_MIN = 1490;//base min (1480)
const int V2_MAX = 1600;//fore max (1700)
const int V2_MIN = 1400;//fore min (1300)
const int V_NEUTRAL = 1500;
const unsigned char END_SIGNAL  = 10;//signal to end transmission of data
const double Kp1 = 50.0;//base arm
const double Kd1 = -50.0;//base earm (-20.0)
const double Kp2 = 250;//base arm
const double Kd2 = -150.0;//forearm (-100)
const double SPEED_TOL = 0.01;//m/s
const double HOME_POS[2] = {0,0.3};//home position in meters (x,y)

//global serial
int cport_nr,        /* /dev/ttyS0 (COM1 on windows) */
bdrate;       /* 115200 baud */

//global variables
double currentTheta1 = M_PI/2.0; //store the counter variable of the encoder from zero
double currentTheta2 = 0.0;
double desiredTheta1 = M_PI/2.0;
double desiredTheta2 = 0.0;
int indicator = 0;//0 = phidget not connected, 1 = phidget connected
double time1[3] = {0,0,1};//holds previous elapsed time
double time2[3] = {0,0,1};
double thetaStore1[3] = {0,0,0};//holds previous positions
double thetaStore2[3] = {0,0,0};
double wcurrent1 = 0.0;
double wcurrent2 = 0.0;
double error1 = 0.0;
double error2 = 0.0;
char vOut1[] = "1500";//pwm ctrl signal for base motor
char vOut2[] = "1500";//pwm ctrl signal for forearm motor

bool runMotorController = false;
bool killThread = false;
bool cameraTracking = false;

string malletX = boost::lexical_cast<string>(int((Len1*cos(currentTheta1)+Len2*cos(currentTheta1+currentTheta2))*100.0));
string malletY = boost::lexical_cast<string>(int((Len1*sin(currentTheta1)+Len2*sin(currentTheta1+currentTheta2))*100.0));
string puckX = boost::lexical_cast<string>(0);
string puckY = boost::lexical_cast<string>(0);
string projectedX = boost::lexical_cast<string>(0);
string projectedY = boost::lexical_cast<string>(0);

double tableLength = 1.36;//meters
double tableWidth = 0.6;//meters
double armOffset = 0.12;//meters

//calibration points, each contains [pixel X, pixel Y, real X, real Y]
double calibratePoint1[4] = {0,0,0,0}; //bottom left point
double calibratePoint2[4] = {0,0,0,0}; //bottom right point
double calibratePoint3[4] = {0,0,0,0}; //top left point
double calibratePoint4[4] = {0,0,0,0}; //top right point
double calibrationXSlopePX[2] = {0.0,0.0}; //scaling factor, left/right side, for pixel line
double calibrationXConstantPX[2] = {0.0,0.0}; //constant, left/right side, for pixel line
double calibrationXSlope = 0.0; //scaling factor, m, xreal/xpx
double calibrationXConstant = 0.0; //constant, b, xreal/xpx
double calibrationYSlope = 0.0; //scaling factor, m, yreal/ypx
double calibrationYConstant = 0.0; //constant, b, yreal/ypx

int encoderPosition[2] = {0,0};

//tracking puck
double puckTime[3] = {0,0,0};
double puckPositionX[3] = {0,0,0};
double puckPositionY[3] = {0,0,0};
double puckVelocity[2] = {0,0};//x,y unit vector

bool runCalcPuckTrajectory = false;//do not run calc puck trajectory thread

//velocity controller variables
double lambda = 5.0;
double x[2] = {0,0}, x_d[2] = {0,0};
double xdot_r[2] = {0,0}, qdot_r[2] = {0,0}, xdot_d[2] = {0,0};
double R = 0.15;//meters
double c = .5;//speed factor
double t = 0;//time in seconds

int output1 = 1500;
int output2 = 1500;
