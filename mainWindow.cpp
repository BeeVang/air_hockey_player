/*
 ============================================================================
 Name        : mainWindow.cpp
 Author      : Bee
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in gtkmm
 ============================================================================
 */
//includes
#include "mainWindow.h"
#include "function.h"
using namespace std;



mainWindow::mainWindow()
: errorMsg(true),
  m_Table1(2,2,true),//main table
  m_Table2(1,2,true),//split top right corner into halves
  m_Table3(3,1,true),//button tables
  m_Table4(4,2,false),//status indicators table
  m_tableAdvSetting(4,1,false),//advance settings
  m_tableSetting(2,2,false),//table for m_frameConcSetting
  m_tableStatus(6,4,true),//table for status indicators
  m_tableArmSetting(3,3,false),
  m_tableTableSetting(3,2,false),
  m_tableCameraSetting(3,1,false),
  m_tableControl(3,4,false),
  m_button2("Status"),
  m_buttonCameraCalibrate("Calibrate Position"),
  m_buttonCalibrateHSV("Calibrate HSV"),
  m_buttonStartCamera("Start Camera Tracking"),
  m_buttonGoToPos("Go to X,Y"),
  m_labelMalletX("X coord"),
  m_labelMalletY("Y coord"),
  m_labelPuckX("X coord"),
  m_labelPuckY("Y coord"),
  m_labelProjX("X coord"),
  m_labelProjY("Y coord"),
  m_labelGoX("X (cm): "),
  m_labelGoY(" Y (cm): "),
  m_labelSetX("X (cm): "),
  m_labelSetY("Y (cm): "),
  m_labelTableLength("Table Length (m): "),
  m_labelTableWidth("Table Width (m): "),
  m_labelArmOffset("Arm offset (m): "),
  m_toggleConnect("Connect"),
  m_toggleMode("Start Robot"),
  m_button15("Help"),
  m_buttonHelpOk("Ok"),
  m_buttonSetTheta1("Set x,y (cm)"),
  m_buttonSetTheta2("Set Theta2 = 0"),
  m_textSave("Save Log"),
  m_frameStatus("Status"),
  m_frameConcSetting("Connection Settings"),
  m_frameArmSetting("Arm Calibration"),
  m_frameTableSetting("Table Settings"),
  m_frameCameraSetting("Camera Calibration"),
  m_frameControl("User Control"),
  m_labelComport("Comport:"),
  m_labelBaudrate("Baudrate: "),
  m_labelArmPos("Mallet Position (cm)"),
  m_labelPuckPos("Puck Position (cm)"),
  m_labelProjectedPos("Projected Impact Position (cm)"),
  m_tableTemp(2,2,false),
  m_imageMallet("./test.jpg"),
  m_imagePuck("./test.jpg"),
  m_imageGuess("./test.jpg"),
  m_imageControl("./img/ControlButtons.jpg")

{
	// This just sets the title of our new window.
	set_title("Air Hockey Controller");
	//set_position(GTK_WIN_POS_CENTER);//work on this later

	// sets the border width of the window.
	set_border_width(10);

	//Action Log
	m_textView1.set_editable(false);
	m_textView1.set_cursor_visible(false);
	m_textBuffer1 = Gtk::TextBuffer::create();
	m_textBuffer1->set_text("**AirHockey Computer Controller Version 1.00** \n");
	m_textView1.set_buffer(m_textBuffer1);

	//define Vbox for Action Log
	m_VBox.pack_start(m_scrolledWindow);
	m_VBox.pack_start(m_textSave, Gtk::PACK_SHRINK);

	//settings section
	m_comboComport.append_text("1");
	m_comboComport.append_text("2");
	m_comboComport.append_text("3");
	m_comboComport.append_text("4");
	m_comboComport.append_text("5");
	m_comboComport.append_text("6");
	m_comboComport.append_text("7");
	m_comboComport.append_text("8");
	m_comboComport.append_text("9");
	m_comboComport.append_text("10");
	m_comboComport.append_text("11");
	m_comboComport.append_text("12");
	m_comboComport.set_active_text("3");
	m_comboBaudrate.append_text("300");
	m_comboBaudrate.append_text("1200");
	m_comboBaudrate.append_text("2400");
	m_comboBaudrate.append_text("4800");
	m_comboBaudrate.append_text("9600");
	m_comboBaudrate.append_text("14400");
	m_comboBaudrate.append_text("19200");
	m_comboBaudrate.append_text("28800");
	m_comboBaudrate.append_text("38400");
	m_comboBaudrate.append_text("57600");
	m_comboBaudrate.append_text("115200");
	m_comboBaudrate.set_active_text("115200");
	m_tableSetting.attach(m_labelComport,0,1,0,1);
	m_tableSetting.attach(m_labelBaudrate,1,2,0,1);
	m_tableSetting.attach(m_comboComport,0,1,1,2,Gtk::FILL,Gtk::FILL,5,5);
	m_tableSetting.attach(m_comboBaudrate,1,2,1,2,Gtk::FILL,Gtk::FILL,5,5);
	m_frameConcSetting.add(m_tableSetting);
	m_frameConcSetting.set_border_width(5);

	//add lables to frame
	m_frameMalletX.add(m_labelMalletX);
	m_frameMalletY.add(m_labelMalletY);
	m_framePuckX.add(m_labelPuckX);
	m_framePuckY.add(m_labelPuckY);
	m_frameProjX.add(m_labelProjX);
	m_frameProjY.add(m_labelProjY);
	m_frameMalletX.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_frameMalletX.set_border_width(2);
	m_frameMalletY.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_frameMalletY.set_border_width(2);
	m_framePuckX.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_framePuckX.set_border_width(2);
	m_framePuckY.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_framePuckY.set_border_width(2);
	m_frameProjX.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_frameProjX.set_border_width(2);
	m_frameProjY.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_frameProjY.set_border_width(2);

	//define table 4 widgets
	m_Table4.attach(m_frameConcSetting,0,2,0,1);//settings section
	m_tableStatus.attach(m_labelArmPos,0,3,0,1);
	m_tableStatus.attach(m_imageMallet,3,4,0,1);
	m_tableStatus.attach(m_frameMalletX,0,2,1,2);
	m_tableStatus.attach(m_frameMalletY,2,4,1,2);
	m_tableStatus.attach(m_labelPuckPos,0,3,2,3);
	m_tableStatus.attach(m_imagePuck,3,4,2,3);
	m_tableStatus.attach(m_framePuckX,0,2,3,4);
	m_tableStatus.attach(m_framePuckY,2,4,3,4);
	m_tableStatus.attach(m_labelProjectedPos,0,3,4,5);
	m_tableStatus.attach(m_imageGuess,3,4,4,5);
	m_tableStatus.attach(m_frameProjX,0,2,5,6);
	m_tableStatus.attach(m_frameProjY,2,4,5,6);
	m_frameStatus.add(m_tableStatus);
	m_frameStatus.set_border_width(5);
	m_Table4.attach(m_frameStatus,0,1,1,4);
	//m_frameTest.set_shadow_type(Gtk::SHADOW_IN);

	//define table 3 widgets
	m_Table3.attach(m_toggleConnect,0,1,0,1);
	m_Table3.attach(m_toggleMode,0,1,1,2);
	m_Table3.attach(m_button15,0,1,2,3);

	//define table 2 widgets
	m_Table2.attach(m_Table4,0,1,0,1);
	m_Table2.attach(m_Table3,1,2,0,1);

	//add text box to scroll window
	m_scrolledWindow.add(m_textView1);
	m_scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);

	// put the box into the main window.
	add(m_Table1);
	m_Table1.attach(m_VBox,0,1,0,1);
	m_Table1.attach(m_tableAdvSetting,0,2,1,2);
	//m_Table1.attach(m_frameDisplay,0,2,1,2);
	//m_frameDisplay.set_border_width(3);
	m_Table1.attach(m_Table2,1,2,0,1);

	//table settings
	m_tableTableSetting.set_border_width(3);
	m_tableTableSetting.attach(m_labelTableLength,0,1,0,1);
	m_tableTableSetting.attach(m_labelTableWidth,0,1,1,2);
	m_tableTableSetting.attach(m_labelArmOffset,0,1,2,3);
	m_tableTableSetting.attach(m_inputLength,1,2,0,1);
	m_tableTableSetting.attach(m_inputWidth,1,2,1,2);
	m_tableTableSetting.attach(m_inputOffset,1,2,2,3);
	m_inputLength.set_text(boost::lexical_cast<string>(tableLength));
	m_inputWidth.set_text(boost::lexical_cast<string>(tableWidth));
	m_inputOffset.set_text(boost::lexical_cast<string>(armOffset));

	//add control settings to bottom of controller table
	m_tableAdvSetting.attach(m_frameArmSetting,0,1,0,1);
	m_tableAdvSetting.attach(m_frameTableSetting,1,2,0,1);
	m_tableAdvSetting.attach(m_frameCameraSetting,2,3,0,1);
	m_tableAdvSetting.attach(m_frameControl,3,4,0,1);
	m_frameArmSetting.set_border_width(3);
	m_frameTableSetting.set_border_width(3);
	m_frameCameraSetting.set_border_width(3);
	m_frameControl.set_border_width(3);
	m_frameArmSetting.add(m_tableArmSetting);
	m_frameTableSetting.add(m_tableTableSetting);
	m_frameCameraSetting.add(m_tableCameraSetting);
	m_frameControl.add(m_tableControl);
	m_labelArmSetting.set_text("Set the end effector position. (Units are in cm)");
	m_labelArmSetting.set_line_wrap(true);
	m_labelArmSetting.set_alignment(Gtk::ALIGN_LEFT);
	m_labelCameraSetting.set_text("Calibrate the camera after setting the initial angles, this button will move the arm to four positions to calibrate the camera.");
	m_labelCameraSetting.set_line_wrap(true);
	m_labelCameraSetting.set_alignment(Gtk::ALIGN_LEFT);
	m_tableArmSetting.attach(m_labelArmSetting,0,3,0,1);
	m_tableArmSetting.set_border_width(3);
	m_tableArmSetting.attach(m_buttonSetTheta1,2,3,1,3);
	//m_tableArmSetting.attach(m_buttonSetTheta2,1,2,2,3);
	m_tableArmSetting.attach(m_labelSetX,0,1,1,2);
	m_tableArmSetting.attach(m_labelSetY,0,1,2,3);
	m_tableArmSetting.attach(m_inputDialogTheta1,1,2,1,2);
	m_tableArmSetting.attach(m_inputDialogTheta2,1,2,2,3);
	m_tableCameraSetting.attach(m_labelCameraSetting,0,2,0,1);
	m_tableCameraSetting.attach(m_buttonCameraCalibrate,0,1,1,2);
	m_tableCameraSetting.attach(m_buttonCalibrateHSV,0,1,2,3);
	m_tableCameraSetting.set_border_width(3);
	m_tableControl.attach(m_buttonStartCamera,0,4,0,1);
	m_tableControl.attach(m_labelGoX,0,1,1,2);
	m_tableControl.attach(m_inputDialogX,1,2,1,2);
	m_tableControl.attach(m_labelGoY,2,3,1,2);
	m_tableControl.attach(m_inputDialogY,3,4,1,2);
	m_tableControl.attach(m_buttonGoToPos,0,4,2,3);
	m_tableControl.set_border_width(3);
	/*m_inputDialogTheta1.set_text(boost::lexical_cast<string>(17));
	m_inputDialogTheta2.set_text(boost::lexical_cast<string>(63));*/

	// Now when the button is clicked, we call the "on_button_clicked" function
	// with a pointer to "button 1" as it's argument
	//m_button1.signal_clicked().connect(sigc::bind<Glib::ustring>(
	//sigc::mem_fun(*this, &HelloWorld::on_button_clicked), "button 1"));

	m_textSave.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::save_log));
	m_button15.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::help));
	m_toggleConnect.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::connect_button));
	m_toggleMode.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::start_robot));
	m_button2.signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
			sigc::mem_fun(*this, &mainWindow::on_button_clicked), "button 2"));
	this->add_events(Gdk::KEY_PRESS_MASK);
	this->signal_key_press_event().connect( sigc::mem_fun( *this, &mainWindow::onKeyPress ) );
	m_buttonCameraCalibrate.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::calibrateCamera));
	m_buttonCalibrateHSV.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::calibrateHSV));
	m_buttonStartCamera.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::startCameraThread));
	m_buttonGoToPos.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::goToXY));
	m_buttonSetTheta1.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::setXYPos));
	m_buttonSetTheta2.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::setTheta2));
	m_buttonHelpOk.signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::helpHide));
	m_threadPosDisplay_Dispatcher.connect(sigc::mem_fun(*this,&mainWindow::updatePosDisplay));

	show_all_children();

	label1.set_text("Manual Mode:");
	label1.modify_font(Pango::FontDescription("Sans Bold Not-Rotated 24"));
	label2.set_text("text for manual mode");
	label3.set_text("Automatic Mode:");
	label3.modify_font(Pango::FontDescription("Sans Bold Not-Rotated 24"));
	label4.set_text(" 1) Press 'Connect' \n 2) Press 'Start Robot' \n 3) Press ' Start Camera Tracking'");
	dialog.add_action_widget(m_buttonHelpOk,0);
	dialog.set_title("Help");
	//dialog.set_default_size(300,100);
	dialog.set_resizable(false);
	m_tableTemp.attach(label1,0,1,0,1);
	m_tableTemp.attach(m_imageControl,0,1,1,2,Gtk::FILL,Gtk::FILL,10,10);
	m_tableTemp.attach(label3,1,2,0,1);
	m_tableTemp.attach(label4,1,2,1,2);

	Gtk::Box *box = dialog.get_vbox();
	box->pack_start(m_tableTemp);
	dialog.show_all_children();
	dialog.set_position(Gtk::WIN_POS_CENTER);
	// Show the widgets.
	// They will not really be shown until this Window is shown.

	//set_resizable(false);
	set_default_size(800,600);
	set_position(Gtk::WIN_POS_CENTER);

	m_threadPosDisplay = Glib::Thread::create(sigc::mem_fun(*this, &mainWindow::updatePositions),true);
}

mainWindow::~mainWindow()
{
	//save position files
	ofstream myfile ("positionConfig.ini");
	if (myfile.is_open())
	{
		//save calibration points
		myfile << calibratePoint1[0] <<"\n";
		myfile << calibratePoint1[1] <<"\n";
		myfile << calibratePoint1[2] <<"\n";
		myfile << calibratePoint1[3] <<"\n";
		myfile << calibratePoint2[0] <<"\n";
		myfile << calibratePoint2[1] <<"\n";
		myfile << calibratePoint2[2] <<"\n";
		myfile << calibratePoint2[3] <<"\n";
		myfile << calibratePoint3[0] <<"\n";
		myfile << calibratePoint3[1] <<"\n";
		myfile << calibratePoint3[2] <<"\n";
		myfile << calibratePoint3[3] <<"\n";
		myfile << calibratePoint4[0] <<"\n";
		myfile << calibratePoint4[1] <<"\n";
		myfile << calibratePoint4[2] <<"\n";
		myfile << calibratePoint4[3] <<"\n";

		//save slopes
		myfile << calibrationYSlope <<"\n";
		myfile << calibrationYConstant <<"\n";
		myfile << calibrationXSlopePX[0] <<"\n";
		myfile << calibrationXSlopePX[1] <<"\n";
		myfile << calibrationXConstantPX[0] <<"\n";
		myfile << calibrationXConstantPX[1] <<"\n";

		//save current theta/encoder position
		myfile << currentTheta1 <<"\n";
		myfile << currentTheta2 <<"\n";

		myfile.close();
		cout << "Position Calibration file updated." << endl;
	}
	else
	{
		cout << "Cannot write to position Calibration file." << endl;
	}
}


